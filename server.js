var channels = [];
var onlineUsers = {}; // mapping of uuid to server channelId
var offlineChats = {}; // mapping of uuid (of offline user) to channelIds
var myUuid = 'FitaborateChatServer';

var pubnub = require('pubnub').init({
    publish_key: 'pub-c-5b6ec1a2-6c8c-456d-b5b5-bca881634a41',
    subscribe_key: 'sub-c-d6868528-432b-11e4-b22e-02ee2ddab7fe',
                  uuid: myUuid
              });

var openChannel = function(res, uuid) {
    
    //generate channel id
    var channelId = 'chatServerChannel-'+(Math.floor((Math.random() * 100000) + 1));
    
    // Subscribe to a channel with presence enabled
    pubnub.subscribe({
        channel: channelId,
        presence: function (m) {
            console.log('[Pubnub] Presence event received: ' + JSON.stringify(m));
            if (m.action) {
                if (m.action === 'join') {
                    cb_userJoinEvent(m);
                }
                else if (m.action === 'leave' || m.action === 'timeout') {
                    cb_userLeaveEvent(m);
                }
                else if (m.action === 'state-change') {
                    cb_userStatusEvent(m);
                }
            }
        },
        message: function (message, env, channel) {
            
            if (message.uuid === myUuid)
                return;
            
            console.log('[Pubnub] msg: ' + JSON.stringify(message) + ', env: ' + env + ', channel: ' + channel);

            console.log('[Pubnub] message rcvd: ' + JSON.stringify(message));
            
            // handle user request
            userMsgHandler(message, channel);
        },
        connect : function () {
            // callback called on a successful connection to pubnub cloud

            // add channel to list of subscribed channels 
            channels.push(channelId);

            onlineUsers[uuid] = {
                serverChannel: channelId,
                state: {}
            };
                        
            console.log('[Pubnub] Joined room: ' + channelId);
            
            res.writeHead(200, {'Content-Type': 'application/json'});
            //res.end( JSON.stringify({channel: channelId}));
            res.end(JSON.stringify({channel: channelId,
                chatChannel: uuid + '-chatListChannel',
                notificatnChannel: uuid + '-notificatnChannel'}));
        },
        error :  function () {
            // callback called on an error event
            console.log('[Pubnub] Error in joinRoom');
        }
    });
    
    var userMsgHandler = function (msg, channel) {
    
        var message = msg.message;
                
        // check the action and act
        if (message.action === 'getPresence') {
            handleGetPresence(message.uuid, channel);
        }
        else if (message.action === 'setupChat') {
            handleSetupChat(channel);
        }
        else if (message.action === 'addUserToChat') {
            handleAddUserToChat(message.uuid, message.channel);
        }
        else if (message.action === 'removeOfflineChat') {
            handleRemoveOfflineChat(msg.uuid, message.channel);
        }
        else {
            console.log('unhandled user request :'+JSON.stringify(message));
        }
                      
    } ;
        
    var cb_userLeaveEvent = function (m) {
        if (m.uuid === myUuid)
            return;
        
        pubnub.unsubscribe({
            channel: channelId
        });

        var index = channels.indexOf(channelId);
        if (-1 !== index) {
            channels.splice(index, 1);
        }
        // remove uuid from online users list 
        delete onlineUsers[m.uuid];
        
        console.log('[Pubnub] Left Room '+ channelId);
       
    };
    
    var cb_userJoinEvent = function (m) {
        if (m.uuid === myUuid)
            return;
        
        onlineUsers[m.uuid].state = m.data;
        
        if (offlineChats.hasOwnProperty(m.uuid)) {
        var msg = {
                action: 'offlineUserChat',
                channel: offlineChats[m.uuid]
        };
        
        sendMsg(channelId, msg);
        }
    };
    
    var cb_userStatusEvent = function (m) {
        if (m.uuid === myUuid)
            return;
        
        onlineUsers[m.uuid].state = m.data;
    };

};
        
var handleGetPresence = function (uuids, channel) {
    var response = {
        action: 'presenceInfo',
        status: []
    };
            
    for (var i = 0; i < uuids.length; i++) {
        if (onlineUsers.hasOwnProperty(uuids[i])) {
            response.status.push({uuid: uuids[i], state: onlineUsers[uuids[i]].state, presence: 'online'});
        } else {
            response.status.push({uuid: uuids[i], state: {}, presence: 'offline'});
                }
            }
    
    sendMsg(channel, response);
};

var handleSetupChat = function (channel) {
    //generate channel id
    var chatChannel = 'chatChannel-' + (Math.floor((Math.random() * 100000) + 1));

    var response = {
        action: 'joinUserChat',
        channel: chatChannel
    };

    sendMsg(channel, response);
};

var handleAddUserToChat = function (uuids, chatChannelId) {
    var uuid;
    var response = {
        action: 'joinUserChat',
        channel: chatChannelId
    };
    
    for (var i =0; i < uuids.length; i++) {
        uuid = uuids[i];
        if (onlineUsers.hasOwnProperty(uuid)) {
            sendMsg(onlineUsers[uuid].serverChannel, response);
    }
        else {
            // user is offline
            if ( !offlineChats[uuid]) {
                offlineChats[uuid] = [];
            }

            offlineChats[uuid].push(chatChannelId);
        }
    }
};

var handleRemoveOfflineChat = function (uuid, channels) {
    var userOfflineChats = offlineChats[uuid];
    if ( !userOfflineChats) {
        console.log('No offline chats found for user '+uuid);
        return;
    }
    
    for (var j = 0; j < channels.length; j++) {
        var channel = channels[j];
    
        for (var i = 0; i < userOfflineChats.length; i++) {
            if (userOfflineChats[i] === channel) {
                userOfflineChats.splice(i, 1);
                console.log('Removed offline chat entry: '+channel);
                break;
            }    
        }        
    }
    
    if (userOfflineChats.length === 0) {
        delete offlineChats[uuid];
    }    
};

var sendMsg = function (channel, message) {
    pubnub.publish({
        channel: channel,
        message: {message: message, uuid:myUuid},
        callback: function (m) {
            console.log('[Pubnub] sendMsg Success: ' + JSON.stringify(m));
        },
        error: function (m) {
            // callback called on an error event
            console.log('[Pubnub] Error in sendMsg: ' + JSON.stringify(m));
        }
    });
};

var http = require('http');

var webServer = http.createServer(function (req, res) {
    
    console.log('Received req with method: '+ req.method); 
    // Set CORS headers
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
    res.setHeader('Access-Control-Allow-Headers', 'X-social-uuid', 'X-current-time-utc', 'X-authentication-sig');
    if (req.method === 'OPTIONS') {
        res.writeHead(200);
        res.end();
    }
    // if method is get - generate channel id and send back in response
    else if (req.method === 'GET') {
        // save uuid 
        var uuid = req.headers['x-social-uuid'];
      
        openChannel(res, uuid);
    } else {
        console.log('Ignoring Request method '+req.method);
    }
});

webServer.listen(1337);
console.log('Server running at port 1337');


